# FixeAds Test

## Rationale
This solution has been developed based on one of the many CSS frameworks out there, the chosen one is called [base](http://matthewhartman.github.io/base/).
And as Base has its styles in LESS and well modularized, it enabled me to choose what modules to import.

Gulp was choosen to be the front-end automation tool.