var gulp = require('gulp'),
    watch = require('gulp-watch'),
    less = require('gulp-less'),
    path = require('path'),
    notify = require('gulp-notify'),
    replace = require('gulp-replace');

gulp.task('less', function () {
  gulp.src('./src/less/style.less')
  .pipe(less({ paths: [ path.join(__dirname, 'less', 'includes') ]})
  .on('error', function(err) {
    this.emit('end');
  }))
  .on("error", notify.onError(function(error) {
    return "Failed to Compile LESS: " + error.message;
  }))
  .pipe(gulp.dest('./src/css'))
  .pipe(notify("LESS Compiled Successfully :)"));
});

gulp.task('watch', function () {
  gulp.watch('src/less/**/*', ['less']);
});

gulp.task('default', ['watch']);